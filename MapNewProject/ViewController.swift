//
//  ViewController.swift
//  MapNewProject
//
//  Created by Mobile Developer on 1/11/18.
//  Copyright © 2018 Mobile Developer. All rights reserved.
//

import UIKit
import ArcGIS
class ViewController: UIViewController {

    @IBOutlet weak var mapView: AGSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.map = AGSMap(basemap: AGSBasemap.imagery())
        self.mapView.locationDisplay.start()
   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onStartNavigation(_ sender: UIBarButtonItem) {
      
            self.mapView.locationDisplay.stop()
            self.mapView.locationDisplay.autoPanMode = .recenter
            self.mapView.locationDisplay.start()
        
    }
    
    @IBAction func onClickCompass(_ sender: UIBarButtonItem) {
        self.mapView.locationDisplay.autoPanMode = .compassNavigation
        self.mapView.locationDisplay.start()
    }
    
    @IBAction func onClickNavigation(_ sender: UIBarButtonItem) {
        self.mapView.locationDisplay.autoPanMode = .navigation
        self.mapView.locationDisplay.start()
    }
    
    
    @IBAction func onStopClick(_ sender: UIBarButtonItem) {
        self.mapView.locationDisplay.stop()
    }
    
    @IBAction func onClickOn(_ sender: UIBarButtonItem) {
        self.mapView.locationDisplay.autoPanMode = .off
        self.mapView.locationDisplay.start()
        
    }
    
    

}

