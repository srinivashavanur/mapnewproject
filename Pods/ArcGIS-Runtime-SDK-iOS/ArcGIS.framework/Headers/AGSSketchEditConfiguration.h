/*
 COPYRIGHT 2016 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <Foundation/Foundation.h>

/** @file AGSSketchEditConfiguration.h */ //Required for Globals API doc

/**  Represents edit configuration for `AGSSketchEditor`.
 
 Instances of this class represent edit configuration for  `AGSSketchEditor`.
 
 @since 100.2
 */
@interface AGSSketchEditConfiguration : NSObject

/** Indicates whether a part in a multipart geometry (multipoint, polyline, or polygon) can be selected.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL allowPartSelection;

/** Indicates whether or not a vertex or a part must first be selected before it can be dragged. Defaults to true.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL requireSelectionBeforeDrag;

/** Mode specifying what actions are permitted on vertices
 @since 100.2
 */
@property (nonatomic, assign, readwrite) AGSSketchVertexEditMode vertexEditMode;

#if TARGET_OS_IPHONE && !TARGET_OS_TV
/** Indicates whether or not a context menu should be displayed when users double-tap a vertex or a part.
 @since 100.2
*/
@property (nonatomic, assign, readwrite, getter=isContextMenuEnabled) BOOL contextMenuEnabled;
#endif

@end

